package org.hmti.SpringCourse.config.pk.model.dto.projection.repository.serv.impl.controller;

import io.swagger.annotations.ApiOperation;
import java.util.List;
import org.hmti.SpringCourse.config.pk.LeaderPk;
import org.hmti.SpringCourse.config.pk.model.Leader;
import org.hmti.SpringCourse.config.pk.model.dto.projection.repository.serv.impl.LeaderServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author asdin
 */
@RestController
@RequestMapping("/leader")
public class LeaderController {

    @Autowired
    private LeaderServiceImpl leaderServiceImpl;

    @GetMapping
    @ApiOperation(httpMethod = "GET", response = List.class,
            value = "This url to get all data leader")
    public List<Leader> findAll() {
        return leaderServiceImpl.findAll();
    }

    @GetMapping("/id")
    @ApiOperation(httpMethod = "GET", response = List.class,
            value = "This url to get leader by id")
    public Leader findById(LeaderPk leaderPk) {
        return leaderServiceImpl.findById(leaderPk);
    }

    @GetMapping("/name")
    @ApiOperation(httpMethod = "GET", response = List.class,
            value = "This url to get leader by name")
    public List<Leader> findByLeaderName(
            @RequestParam(defaultValue = "leader name", required = true,
                    value = "This leader name will be search") String leaderName) {
        return leaderServiceImpl.findByLeaderName(leaderName);
    }

    @PostMapping
    @ApiOperation(httpMethod = "POST", response = List.class,
            value = "This url to add data leader")
    public void save(@RequestBody Leader leader) {
        leaderServiceImpl.save(leader);
    }

    @PutMapping
    @ApiOperation(httpMethod = "PUT", response = List.class,
            value = "This url to update data leader")
    public void update(@RequestBody Leader leader) {
        leaderServiceImpl.update(leader);

    }

    @DeleteMapping
    @ApiOperation(httpMethod = "DELETE", response = List.class,
            value = "This url to delete data leader by id")
    public void delete(LeaderPk leaderPk) {
        leaderServiceImpl.delete(leaderPk);
    }
}
