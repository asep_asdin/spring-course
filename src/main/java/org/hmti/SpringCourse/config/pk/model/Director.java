package org.hmti.SpringCourse.config.pk.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hmti.SpringCourse.config.pk.DirectorPk;

@Entity
@Table(name = "director")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Director implements Serializable {

    @EmbeddedId
    private DirectorPk directorPk;
    @Basic(optional = false)
    @Column(unique = true)
    private String employeeNumber;
    private String directorName;
    private String jobDesk;
    @ManyToOne(optional = false)
    @JoinColumn(name = "company_id", referencedColumnName = "company_id")
    private Company companyId;

}
