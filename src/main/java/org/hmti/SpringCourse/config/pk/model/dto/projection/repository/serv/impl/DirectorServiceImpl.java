package org.hmti.SpringCourse.config.pk.model.dto.projection.repository.serv.impl;

import java.util.List;
import org.hmti.SpringCourse.config.pk.DirectorPk;
import org.hmti.SpringCourse.config.pk.model.Director;
import org.hmti.SpringCourse.config.pk.model.dto.projection.repository.DirectorRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

@Service
@Transactional
public class DirectorServiceImpl {
    
    @Autowired
    private DirectorRepo directorRepo;
    
    public List<Director> findAll() {
        return directorRepo.findAll();
    }
    
    public Director findById(DirectorPk directorPk) {
        return directorRepo.findById(directorPk).get();
    }
    
    public List<Director> findByDirectorName(String directorName) {
        return directorRepo.findByDirectorName(directorName);
    }
    
    public void saveOrUpdate(Director director) {
        directorRepo.save(director);
    }
    
    public void save(Director director) {
        if (directorRepo.findById(director.getDirectorPk()).isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "id already exist");
        }
        directorRepo.save(director);
    }
    
    public void update(Director director) {
        if (!directorRepo.findById(director.getDirectorPk()).isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "id not found");
        }
        directorRepo.save(director);
    }
    
    public void delete(DirectorPk directorPk) {
        directorRepo.deleteById(directorPk);
    }
}
