package org.hmti.SpringCourse.config.pk.model.dto.projection.repository.serv.impl;

import java.util.List;
import org.hmti.SpringCourse.config.pk.CompanyPk;
import org.hmti.SpringCourse.config.pk.model.Company;
import org.hmti.SpringCourse.config.pk.model.dto.projection.repository.CompanyRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

@Service
@Transactional
public class CompanyServiceImpl {
    
    @Autowired
    private CompanyRepo companyRepo;
    
    public List<Company> findAll() {
        return companyRepo.findAll();
    }
    
    public Company findById(CompanyPk companyPk) {
        return companyRepo.findById(companyPk).get();
    }
    
    public List<Company> findByCompanyName(String companyName) {
        return companyRepo.findBycompanyName(companyName);
    }
    
    public void saveOrUpdate(Company company) {
        companyRepo.save(company);
    }
    
    public void save(Company company) {
        if (companyRepo.findById(company.getCompanyPk()).isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "id already exist");
        }
        companyRepo.save(company);
    }
    
    public void update(Company company) {
        if (!companyRepo.findById(company.getCompanyPk()).isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "if not found");
        }
        companyRepo.save(company);
    }
    
    public void delete(CompanyPk companyPk) {
        companyRepo.deleteById(companyPk);
    }
    
}
