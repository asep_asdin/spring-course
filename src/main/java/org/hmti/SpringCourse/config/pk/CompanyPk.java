package org.hmti.SpringCourse.config.pk;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CompanyPk implements Serializable {

    @Basic(optional = false)
    @Column(name = "company_id", length = 5)
    private Integer company_id;

}
