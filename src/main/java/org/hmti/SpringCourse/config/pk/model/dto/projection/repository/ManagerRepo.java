package org.hmti.SpringCourse.config.pk.model.dto.projection.repository;

import java.util.List;
import org.hmti.SpringCourse.config.pk.ManagerPk;
import org.hmti.SpringCourse.config.pk.model.Manager;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ManagerRepo extends JpaRepository<Manager, ManagerPk> {

    List<Manager> findByManagerName(String managerName);
}
