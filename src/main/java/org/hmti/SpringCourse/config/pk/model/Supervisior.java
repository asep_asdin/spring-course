package org.hmti.SpringCourse.config.pk.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hmti.SpringCourse.config.pk.SupervisiorPk;

@Entity
@Table(name = "supervisior")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Supervisior implements Serializable {

    @EmbeddedId
    private SupervisiorPk supervisiorPk;
//    @Basic(optional = false)
//    @Column(name = "supervisior_id", length = 10)
//    private Integer supervisior_id;
    @Basic(optional = false)
    @Column(unique = true)
    private String emplyeeId;
    private String supervisiorName;
    private String jobDesk;
    @ManyToOne(optional = false)
    @JoinColumn(name = "manager_id", referencedColumnName = "manager_id")
    private Manager namagerId;

}
