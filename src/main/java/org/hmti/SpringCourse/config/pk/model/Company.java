package org.hmti.SpringCourse.config.pk.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hmti.SpringCourse.config.pk.CompanyPk;

@Entity
@Table(name = "company")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Company implements Serializable {

    @EmbeddedId
    private CompanyPk companyPk;
    @Basic(optional = false)
    @Column(unique = true)
    private String companyName;
    private String companyAddress;
    private String companyType;
}
