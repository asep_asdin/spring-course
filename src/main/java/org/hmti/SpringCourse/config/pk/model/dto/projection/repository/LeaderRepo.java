package org.hmti.SpringCourse.config.pk.model.dto.projection.repository;

import java.util.List;
import org.hmti.SpringCourse.config.pk.LeaderPk;
import org.hmti.SpringCourse.config.pk.model.Leader;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author asdin
 */
@Repository
public interface LeaderRepo extends JpaRepository<Leader, LeaderPk> {

    List<Leader> findByLeaderName(String leaderName);
}
