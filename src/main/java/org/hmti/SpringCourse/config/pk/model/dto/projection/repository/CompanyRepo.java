package org.hmti.SpringCourse.config.pk.model.dto.projection.repository;

import java.util.List;
import org.hmti.SpringCourse.config.pk.CompanyPk;
import org.hmti.SpringCourse.config.pk.model.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyRepo extends JpaRepository<Company, CompanyPk> {

    List<Company> findBycompanyName(String companyName);
}
