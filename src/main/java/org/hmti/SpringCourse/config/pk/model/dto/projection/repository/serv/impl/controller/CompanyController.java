package org.hmti.SpringCourse.config.pk.model.dto.projection.repository.serv.impl.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.List;
import org.hmti.SpringCourse.config.pk.CompanyPk;
import org.hmti.SpringCourse.config.pk.model.Company;
import org.hmti.SpringCourse.config.pk.model.dto.projection.repository.serv.impl.CompanyServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/company")
public class CompanyController {

    @Autowired
    private CompanyServiceImpl companyServiceImpl;

    @GetMapping
    public List<Company> findAll() {
        return companyServiceImpl.findAll();
    }

    @GetMapping("/id")
    @ApiOperation(httpMethod = "GET", response = List.class,
            value = "this url to find company by Id")
    public void findById(CompanyPk companyPk) {
        companyServiceImpl.findById(companyPk);
    }

    @GetMapping("/name")
    @ApiOperation(httpMethod = "GET", response = List.class,
            value = "this url to find company by name")
    public List<Company> findByCompanyName(
            @RequestParam(required = true)
            @ApiParam(defaultValue = "company", required = true,
                    value = "company that will be search") String companyName) {
        return companyServiceImpl.findByCompanyName(companyName);
    }

    @PostMapping("/saveorupdate")
    public void saveOrUpdate(@RequestBody Company company) {
        companyServiceImpl.saveOrUpdate(company);
    }

    @PostMapping
    public void save(@RequestBody Company company) {
        companyServiceImpl.save(company);
    }

    @PutMapping
    public void update(@RequestBody Company company) {
        companyServiceImpl.update(company);
    }

    @DeleteMapping
    public void delete(@RequestBody CompanyPk companyPk) {
        companyServiceImpl.delete(companyPk);
    }
}
