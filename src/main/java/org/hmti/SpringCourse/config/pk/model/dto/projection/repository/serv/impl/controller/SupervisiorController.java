package org.hmti.SpringCourse.config.pk.model.dto.projection.repository.serv.impl.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.List;
import org.hmti.SpringCourse.config.pk.SupervisiorPk;
import org.hmti.SpringCourse.config.pk.model.Supervisior;
import org.hmti.SpringCourse.config.pk.model.dto.projection.repository.serv.impl.SupervisiorServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.server.Http2;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author asdin
 */
@RestController
@RequestMapping("/supervisior")
public class SupervisiorController {

    @Autowired
    private SupervisiorServiceImpl supervisiorServiceImpl;

    @GetMapping
    public List<Supervisior> findAll() {
        return supervisiorServiceImpl.findAll();
    }

    @GetMapping("/id")
    @ApiOperation(httpMethod = "GET", response = List.class,
            value = "this url to get data supervisior by id")
    public Supervisior findbyId(SupervisiorPk supervisiorPk) {
        return supervisiorServiceImpl.findById(supervisiorPk);
    }

    @GetMapping("/name")
    @ApiOperation(httpMethod = "GET", response = List.class,
            value = "this url to get supervisior by name")
    public List<Supervisior> findBySupervisiorName(
            @RequestParam(defaultValue = "supervisior name", required = true,
                    value = "this supervisior name will be search") String supervisiorName) {
        return supervisiorServiceImpl.findBySupervisiorName(supervisiorName);
    }

    @PostMapping
    @ApiOperation(httpMethod = "POST", response = List.class,
            value = "this url to add supervisior")
    public void save(@RequestBody Supervisior supervisior) {
        supervisiorServiceImpl.save(supervisior);
    }

    @PutMapping
    @ApiOperation(httpMethod = "PUT", response = List.class,
            value = "this url to update supervisior")
    public void update(@RequestBody Supervisior supervisior) {
        supervisiorServiceImpl.save(supervisior);
    }

    @DeleteMapping
    @ApiOperation(httpMethod = "DELETE", response = List.class,
            value = "this url to delete supervisior")
    public void delete(SupervisiorPk supervisiorPk) {
        supervisiorServiceImpl.delete(supervisiorPk);
    }
}
