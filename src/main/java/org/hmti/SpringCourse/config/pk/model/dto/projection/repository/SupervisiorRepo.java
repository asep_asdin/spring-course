
package org.hmti.SpringCourse.config.pk.model.dto.projection.repository;

import java.util.List;
import org.hmti.SpringCourse.config.pk.SupervisiorPk;
import org.hmti.SpringCourse.config.pk.model.Supervisior;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SupervisiorRepo extends JpaRepository<Supervisior, SupervisiorPk>{
    List<Supervisior> findBySupervisiorName(String supervisiorName);
}
