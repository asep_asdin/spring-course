
package org.hmti.SpringCourse.config.pk.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hmti.SpringCourse.config.pk.LeaderPk;

@Entity
@Table(name = "leader")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Leader implements Serializable {
    @EmbeddedId
    private LeaderPk leaderPk;
    @Basic(optional = false)
    @Column(unique = true)
    private String employeeId;
    private String leaderName;
    private String leaderDivision;
    private String jobDesc;
    @ManyToOne(optional = false)
    @JoinColumn(name = "supervisior_id", referencedColumnName = "supervisior_id")
    private Supervisior supervisiorId;
}
