package org.hmti.SpringCourse.config.pk;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SupervisiorPk implements Serializable {

    @Basic(optional = false)
    @Column(name = "supervisior_id", length = 10)
    private Integer supervisior_id;
}
