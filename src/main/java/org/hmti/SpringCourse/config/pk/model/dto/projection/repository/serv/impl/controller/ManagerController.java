package org.hmti.SpringCourse.config.pk.model.dto.projection.repository.serv.impl.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.List;
import org.hmti.SpringCourse.config.pk.ManagerPk;
import org.hmti.SpringCourse.config.pk.model.Manager;
import org.hmti.SpringCourse.config.pk.model.dto.projection.repository.serv.impl.ManagerServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/manager")
public class ManagerController {

    @Autowired
    private ManagerServiceImpl managerServiceImpl;

    @GetMapping
    public List<Manager> findAll() {
        return managerServiceImpl.findAll();
    }

    @GetMapping("/id")
    @ApiOperation(httpMethod = "GET", response = List.class,
            value = "this url to find manager by id")
    public Manager findById(ManagerPk managerPk) {
        return managerServiceImpl.findById(managerPk);
    }

    @GetMapping("/name")
    @ApiOperation(httpMethod = "GET", response = List.class,
            value = "this url to find manager by name")
    public List<Manager> findByMarNanageme(
            @RequestParam(defaultValue = "manager name", required = true,
                    value = "this manager name will be search") String managerName) {
        return managerServiceImpl.findByManagerName(managerName);
    }

    @PostMapping
    public void save(@RequestBody Manager manager) {
        managerServiceImpl.save(manager);
    }

    @PutMapping
    public void update(@RequestBody Manager manager) {
        managerServiceImpl.update(manager);
    }

    @DeleteMapping
    public void delete(@RequestBody ManagerPk managerPk) {
        managerServiceImpl.delete(managerPk);
    }

}
