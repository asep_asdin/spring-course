package org.hmti.SpringCourse.config.pk.model.dto.projection.repository.serv.impl;

import java.util.List;
import org.hmti.SpringCourse.config.pk.LeaderPk;
import org.hmti.SpringCourse.config.pk.model.Leader;
import org.hmti.SpringCourse.config.pk.model.dto.projection.repository.LeaderRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

/**
 *
 * @author asdin
 */
@Service
@Transactional
public class LeaderServiceImpl {
    
    @Autowired
    private LeaderRepo leaderRepo;
    
    public List<Leader> findAll() {
        return leaderRepo.findAll();
    }
    
    public Leader findById(LeaderPk leaderPk) {
        return leaderRepo.findById(leaderPk).get();
    }
    
    public List<Leader> findByLeaderName(String leaderName) {
        return leaderRepo.findByLeaderName(leaderName);
    }
    
    public void save(Leader leader) {
        if (leaderRepo.findById(leader.getLeaderPk()).isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Id Already Exist");
        }
        leaderRepo.save(leader);
    }
    
    public void update(Leader leader) {
        if (!leaderRepo.findById(leader.getLeaderPk()).isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "id not found");
        }
        leaderRepo.save(leader);
    }
    
    public void delete(LeaderPk leaderPk) {
        leaderRepo.deleteById(leaderPk);
    }
}
