package org.hmti.SpringCourse.config.pk.model.dto.projection.repository.serv.impl.controller;

import io.swagger.annotations.ApiOperation;
import java.util.List;
import org.hmti.SpringCourse.config.pk.DirectorPk;
import org.hmti.SpringCourse.config.pk.model.Director;
import org.hmti.SpringCourse.config.pk.model.dto.projection.repository.serv.impl.DirectorServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/director")
public class DirectorController {

    @Autowired
    private DirectorServiceImpl directorServiceImpl;

    @GetMapping
    public List<Director> findAll() {
        return directorServiceImpl.findAll();
    }

    @GetMapping("/id")
    @ApiOperation(httpMethod = "GET", response = List.class,
            value = "this url to find director by id")
    public Director findById(DirectorPk directorPk) {
        return directorServiceImpl.findById(directorPk);
    }

    @GetMapping("/name")
    @ApiOperation(httpMethod = "GET", response = List.class,
            value = "this url to find director by name")
    public List<Director> findByDirectorName(
            @RequestParam(defaultValue = "name director", required = true,
                    value = "this director will be search") String directorName) {
        return directorServiceImpl.findByDirectorName(directorName);
    }

    @PostMapping("/saveorupdate")
    public void saveOrUpdate(@RequestBody Director director) {
        directorServiceImpl.saveOrUpdate(director);
    }

    @PostMapping
    public void save(@RequestBody Director director) {
        directorServiceImpl.save(director);
    }

    @PutMapping
    public void update(@RequestBody Director director) {
        directorServiceImpl.update(director);
    }

    @DeleteMapping
    public void delete(@RequestBody DirectorPk directorPk) {
        directorServiceImpl.delete(directorPk);
    }
}
