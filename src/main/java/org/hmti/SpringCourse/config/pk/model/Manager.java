package org.hmti.SpringCourse.config.pk.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hmti.SpringCourse.config.pk.ManagerPk;

@Entity
@Table(name = "manager")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Manager implements Serializable {

    @EmbeddedId
    private ManagerPk managerPk;
    @Basic(optional = false)
    @Column(unique = true)
    private String employeeId;
    private String managerName;
    private String managerDivision;
    private String jobDesk;
    @ManyToOne(optional = false)
    @JoinColumn(name = "director_id", referencedColumnName = "director_id")
    private Director directorId;
}
