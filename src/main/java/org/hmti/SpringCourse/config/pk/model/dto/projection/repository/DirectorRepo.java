package org.hmti.SpringCourse.config.pk.model.dto.projection.repository;

import java.util.List;
import org.hmti.SpringCourse.config.pk.DirectorPk;
import org.hmti.SpringCourse.config.pk.model.Company;
import org.hmti.SpringCourse.config.pk.model.Director;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DirectorRepo extends JpaRepository<Director, DirectorPk>{

    List<Director> findByDirectorName(String directorName);
}
