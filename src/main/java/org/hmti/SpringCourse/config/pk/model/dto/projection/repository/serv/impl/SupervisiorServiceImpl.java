package org.hmti.SpringCourse.config.pk.model.dto.projection.repository.serv.impl;

import java.util.List;
import org.hmti.SpringCourse.config.pk.SupervisiorPk;
import org.hmti.SpringCourse.config.pk.model.Supervisior;
import org.hmti.SpringCourse.config.pk.model.dto.projection.repository.SupervisiorRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

@Service
@Transactional
public class SupervisiorServiceImpl {

    @Autowired
    private SupervisiorRepo supervisiorRepo;

    public List<Supervisior> findAll() {
        return supervisiorRepo.findAll();
    }

    public Supervisior findById(SupervisiorPk supervisiorPk) {
        return supervisiorRepo.findById(supervisiorPk).get();
    }

    public List<Supervisior> findBySupervisiorName(String supervisiorName) {
        return supervisiorRepo.findBySupervisiorName(supervisiorName);
    }

    public void saveOrUpdate(Supervisior supervisior) {
        supervisiorRepo.save(supervisior);
    }

    public void save(Supervisior supervisior) {
        if (supervisiorRepo.findById(supervisior.getSupervisiorPk()).isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "id already exist");
        }
        supervisiorRepo.save(supervisior);
    }

    public void update(Supervisior supervisior) {
        if (!supervisiorRepo.findById(supervisior.getSupervisiorPk()).isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "id not found");
        }
        supervisiorRepo.save(supervisior);
    }

    public void delete(SupervisiorPk supervisiorPk) {
        supervisiorRepo.deleteById(supervisiorPk);
    }
}
