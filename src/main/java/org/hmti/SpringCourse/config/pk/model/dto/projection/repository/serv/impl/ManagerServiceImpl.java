package org.hmti.SpringCourse.config.pk.model.dto.projection.repository.serv.impl;

import java.util.List;
import org.hmti.SpringCourse.config.pk.ManagerPk;
import org.hmti.SpringCourse.config.pk.model.Manager;
import org.hmti.SpringCourse.config.pk.model.dto.projection.repository.ManagerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

@Service
@Transactional
public class ManagerServiceImpl {
    
    @Autowired
    private ManagerRepo managerRepo;
    
    public List<Manager> findAll() {
        return managerRepo.findAll();
    }
    
    public Manager findById(ManagerPk managerPk) {
        return managerRepo.findById(managerPk).get();
    }
    
    public List<Manager> findByManagerName(String managerName) {
        return managerRepo.findByManagerName(managerName);
    }
    
    public void saveOrUpdate(Manager manager) {
        managerRepo.save(manager);
    }
    
    public void save(Manager manager) {
        if (managerRepo.findById(manager.getManagerPk()).isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "id already exist");
        }
        managerRepo.save(manager);
    }
    
    public void update(Manager manager) {
        if (!managerRepo.findById(manager.getManagerPk()).isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "id not found");
        }
        managerRepo.save(manager);
    }
    
    public void delete(ManagerPk managerPk) {
        managerRepo.deleteById(managerPk);
    }
}
