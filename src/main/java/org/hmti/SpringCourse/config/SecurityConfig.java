
package org.hmti.SpringCourse.config;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

/**
 *
 * @author asdin
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{
    @Autowired
    private PasswordEncoder passwordEncoder;
    
    @Override
    protected void configure(HttpSecurity http) throws Exception{
    http
            .csrf().disable()
            .authorizeRequests().anyRequest().authenticated()
            .and().formLogin().defaultSuccessUrl("/swagger-ui.html")
            .and().logout().deleteCookies("JSESSIONID");
    }
    @Bean
    public InMemoryUserDetailsManager inMemoryUserDetailsManager(){
        List<UserDetails> usersDetail = new ArrayList<>();
        usersDetail.add(User.builder().username("Bambang")
                .password(passwordEncoder.encode("password"))
                .authorities(new SimpleGrantedAuthority("Presiden"))
                .build());
        usersDetail.add(User.builder().username("wiro")
                .password(passwordEncoder.encode("212"))
                .authorities(new SimpleGrantedAuthority("Presiden"))
                .build());
        return new InMemoryUserDetailsManager(usersDetail);
    }
}
